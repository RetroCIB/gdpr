<!-- GDPR CORE: Don't change !!! -->
<?php
$GDPR_OPTIONS = [

    'API' => 'gdpr-api.php',

    'ID' => substr(str_shuffle('0123456789abcdefghijklmnopqrstuvwxyz'), 0, 32),

    'UA' => 'UA-120261035-1',

    'saved' => false,

    'cookies' => [
        'technical' => [
            'label' => 'Cookie-uri tehnice (strict necesare)',
            'name' => 'technical',
            'checked' => true,
            'disabled' => true,
            'cookies_names' => ['_ga', '_gid']
        ],
        'functional' => [
            'label' => 'Cookie-uri de functionalitate',
            'name' => 'functional',
            'checked' => true,
            'disabled' => false,
            'cookies_names' => ['_ga', '_gid']
        ],
        'analytical' => [
            'label' => 'Cookie-uri analitice',
            'name' => 'analytical',
            'checked' => true,
            'disabled' => false,
            'cookies_names' => ['_ga', '_gid']
        ],
        'marketing' => [
            'label' => 'Cookie-uri de marketing',
            'name' => 'marketing',
            'checked' => false,
            'disabled' => false,
            'cookies_names' => ['_ga', '_gid']
        ]
    ],

    'popup' => [
        'url' => 'http://retrocib.com',
        'name' => 'RetroCIB',
        'cookies_policy' => "http://retrocib.com/cookies-policy"
    ],

    'bar' => [
        'title' => 'Acest website foloseste cookies pentru a va oferi cea mai buna experienta de utilizare. Va rugam sa va exprimati preferintele in ceea ce priveste utilizarea cookie-urilor.',
        'more_button' => 'More',
        'save_button' => 'Save',
        'close_button' => 'Close'
    ],

    'bar_saved' => [
        'title' => 'Acest site foloseste cookie',
        'close_button' => 'Close'
    ]
];
?>
<script type="text/javascript">
    window.addEventListener('load', gdpr_inject);

    function gdpr_inject() {

        // functii prelucrare cookies
        var Cookie = {
            "set": function (cookie_name, cookie_value, expire_days) {
                var date = new Date();
                date.setTime(date.getTime() + (expire_days * 24 * 60 * 60 * 1000));
                var expires = "expires=" + date.toUTCString();
                document.cookie = cookie_name + "=" + cookie_value + ";" + expires + ";path=/";
            },
            "get": function (cookie_name) {
                var name = cookie_name + "=";
                var decodedCookie = decodeURIComponent(document.cookie);
                var ca = decodedCookie.split(';');
                for (var i = 0; i < ca.length; i++) {
                    var c = ca[i];
                    while (c.charAt(0) == ' ') {
                        c = c.substring(1);
                    }
                    if (c.indexOf(name) == 0) {
                        return c.substring(name.length, c.length);
                    }
                }
                return "";
            },
            "delete": function (cookie_name) {
                // this.setCookie(cookie_name, "", -1);
                document.cookie = cookie_name + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;path=/';
            }
        };

        // functii/actiuni
        var Actions = {

            "showPopup": function () {
                var gdpr_privacy_popup = document.getElementById("gdpr-privacy-popup");
                if (gdpr_privacy_popup) {
                    gdpr_privacy_popup.style.display = "block";
                }
            },
            "showBar": function () {
                var gdpr_privacy_bar = document.getElementById("gdpr-privacy-bar");
                if (gdpr_privacy_bar) {
                    gdpr_privacy_bar.style.display = "block";
                }
                this.hideBarSaved();
            },
            "showBarSaved": function () {
                var gdpr_privacy_bar_saved = document.getElementById("gdpr-privacy-bar-saved");
                if (gdpr_privacy_bar_saved) {
                    gdpr_privacy_bar_saved.style.display = "block";
                }
                this.hideBar();
            },

            "hidePopup": function () {
                var gdpr_privacy_popup = document.getElementById("gdpr-privacy-popup");
                if (gdpr_privacy_popup) {
                    gdpr_privacy_popup.style.display = "none";
                }
            },
            "hideBar": function () {
                var gdpr_privacy_bar = document.getElementById("gdpr-privacy-bar");
                if (gdpr_privacy_bar) {
                    gdpr_privacy_bar.style.display = "none";
                }

            },
            "hideBarSaved": function () {
                var gdpr_privacy_bar_saved = document.getElementById("gdpr-privacy-bar-saved");
                if (gdpr_privacy_bar_saved) {
                    gdpr_privacy_bar_saved.style.display = "none";
                }
            },

            "hideAll": function () {
                this.hidePopup();
                this.hideBar();
                this.hideBarSaved();
            },

            "isSaved": function () {
                return GDPR.saved == true;
            },

            "toogleCookiesCheck": function (self) {
                GDPR.cookies[self.name].checked = self.checked;
            },

            "saveConsent": function () {

                // construieste valorile pentru localStorage
                var gdpr_cookies = {
                    'saved': true,
                    'ID': GDPR.ID,
                };
                for (var cookie in GDPR.cookies) {
                    var name = GDPR.cookies[cookie].name;
                    var checked = GDPR.cookies[cookie].checked;
                    gdpr_cookies[name] = checked;
                }

                // trimite catre api
                var xhr = new XMLHttpRequest();
                xhr.open("POST", GDPR.API, true);
                xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                xhr.send("action=register_consent&gdpr_consent=" + JSON.stringify(gdpr_cookies));


                // salveaza in localStorage
                localStorage.setItem("gdpr-cookies", JSON.stringify(gdpr_cookies));


                // sterge cookieurile pentru fiecare tip de cookie specificat daca acesta nu e marcat
                for (var cookie in GDPR.cookies) {
                    cookie = GDPR.cookies[cookie];
                    if (cookie.checked == false) {
                        cookie.cookies_names.forEach(function (cookie_name) {
                            Cookie.delete(cookie_name);
                        })
                    }
                }
                // daca e setat cookie pentru analytics dar nu e marcat se dezactiveaza
                if ((GDPR.cookies.analytical) && (!GDPR.cookies.analytical.checked)) {
                    window['ga-disable-' + GDPR.UA] = true;
                }


                GDPR.saved = true;
                GDPR.Actions.hideAll();
            }
        };

        // preia gdpr options si adauga Cookie si Actions
        window.GDPR = JSON.parse('<?= json_encode($GDPR_OPTIONS) ?>');
        GDPR = Object.assign(GDPR, {"Cookie": Cookie});
        GDPR = Object.assign(GDPR, {"Actions": Actions});

        // daca exista cookieuri de prelucrat ..
        if (Object.keys(GDPR.cookies).length > 0) {

            // preia valori din localstorage
            var local_storage_gdpr = JSON.parse(localStorage.getItem('gdpr-cookies'));

            // daca exista setat atunci actualizeaza in GDPR.cookies
            if (local_storage_gdpr !== null) {
                for (var cookie in GDPR.cookies) {
                    GDPR.cookies[cookie].checked = local_storage_gdpr[cookie] || GDPR.cookies[cookie].checked;
                }
                GDPR.saved = local_storage_gdpr['saved'] || GDPR.saved;
                GDPR.ID = local_storage_gdpr['ID'] || GDPR.ID;
            }

            // preia template-urile
            var GDPR_TEMPLATE__PRIVACY_BAR = document.getElementById("GDPR_TEMPLATE__PRIVACY_BAR").innerHTML,
                GDPR_TEMPLATE__PRIVACY_BAR__INPUT = document.getElementById("GDPR_TEMPLATE__PRIVACY_BAR__INPUT").innerHTML,
                GDPR_TEMPLATE__PRIVACY_BAR_SAVED = document.getElementById("GDPR_TEMPLATE__PRIVACY_BAR_SAVED").innerHTML,
                GDPR_TEMPLATE__POPUP = document.getElementById("GDPR_TEMPLATE__POPUP").innerHTML;

            // injecteaza in GDPR_TEMPLATE__POPUP
            GDPR_TEMPLATE__POPUP = GDPR_TEMPLATE__POPUP.replace(new RegExp('#{gdpr-privacy-popup-name}', 'gmi'), GDPR.popup.name);
            GDPR_TEMPLATE__POPUP = GDPR_TEMPLATE__POPUP.replace(new RegExp('#{gdpr-privacy-popup-url}', 'gmi'), GDPR.popup.url);
            GDPR_TEMPLATE__POPUP = GDPR_TEMPLATE__POPUP.replace(new RegExp('#{gdpr-privacy-popup-cookies-policy}', 'gmi'), GDPR.popup.cookies_policy);

            //injecteaza in GDPR_TEMPLATE__PRIVACY_BAR__INPUT pentru fiecare cookie dorit
            var tdpbli_list = "";
            for (var cookie in GDPR.cookies) {
                cookie = GDPR.cookies[cookie];
                var tdpbli = GDPR_TEMPLATE__PRIVACY_BAR__INPUT;
                tdpbli = tdpbli.replace(new RegExp('#{gdpr-privacy-bar-input-label}', 'gmi'), cookie.label);
                tdpbli = tdpbli.replace(new RegExp('#{gdpr-privacy-bar-input-name}', 'gmi'), cookie.name);
                tdpbli = tdpbli.replace(new RegExp('#{gdpr-privacy-bar-input-checked}', 'gmi'), cookie.checked ? 'checked' : '');
                tdpbli = tdpbli.replace(new RegExp('#{gdpr-privacy-bar-input-disabled}', 'gmi'), cookie.disabled ? 'disabled' : '');
                tdpbli_list = tdpbli_list + tdpbli;
            }

            // injecteaza in GDPR_TEMPLATE__PRIVACY_BAR
            GDPR_TEMPLATE__PRIVACY_BAR = GDPR_TEMPLATE__PRIVACY_BAR.replace(new RegExp('#{gdpr-privacy-bar-title}', 'gmi'), GDPR.bar.title);
            GDPR_TEMPLATE__PRIVACY_BAR = GDPR_TEMPLATE__PRIVACY_BAR.replace(new RegExp('#{gdpr-privacy-bar-list}', 'gmi'), tdpbli_list);
            GDPR_TEMPLATE__PRIVACY_BAR = GDPR_TEMPLATE__PRIVACY_BAR.replace(new RegExp('#{gdpr-privacy-bar-more-button}', 'gmi'), GDPR.bar.more_button);
            GDPR_TEMPLATE__PRIVACY_BAR = GDPR_TEMPLATE__PRIVACY_BAR.replace(new RegExp('#{gdpr-privacy-bar-save-button}', 'gmi'), GDPR.bar.save_button);
            GDPR_TEMPLATE__PRIVACY_BAR = GDPR_TEMPLATE__PRIVACY_BAR.replace(new RegExp('#{gdpr-privacy-bar-close-button}', 'gmi'), GDPR.bar.close_button);

            // injecteaza in GDPR_TEMPLATE__PRIVACY_BAR_SAVED
            GDPR_TEMPLATE__PRIVACY_BAR_SAVED = GDPR_TEMPLATE__PRIVACY_BAR_SAVED.replace(new RegExp('#{gdpr-privacy-bar-saved-title}', 'gmi'), GDPR.bar_saved.title);
            GDPR_TEMPLATE__PRIVACY_BAR_SAVED = GDPR_TEMPLATE__PRIVACY_BAR_SAVED.replace(new RegExp('#{gdpr-privacy-bar-close-button}', 'gmi'), GDPR.bar_saved.close_button);

            // injecteaza templaturile in GDPR_TEMPLATE__CONTENT
            var GDPR_TEMPLATE__CONTENT = document.getElementById("GDPR_TEMPLATE__CONTENT");
            GDPR_TEMPLATE__CONTENT.innerHTML += GDPR_TEMPLATE__POPUP;
            GDPR_TEMPLATE__CONTENT.innerHTML += GDPR_TEMPLATE__PRIVACY_BAR;
            GDPR_TEMPLATE__CONTENT.innerHTML += GDPR_TEMPLATE__PRIVACY_BAR_SAVED;


            // sterge cookieurile pentru fiecare tip de cookie specificat daca acesta nu e marcat
            for (var cookie in GDPR.cookies) {
                cookie = GDPR.cookies[cookie];
                if (cookie.checked == false) {
                    cookie.cookies_names.forEach(function (cookie_name) {
                        Cookie.delete(cookie_name);
                    })
                }
            }
            // daca e setat cookie pentru analytics dar nu e marcat se dezactiveaza
            if ((GDPR.cookies.analytical) && (!GDPR.cookies.analytical.checked)) {
                window['ga-disable-' + GDPR.UA] = true;
            }


            //afiseaza bara sau bara de salvat
            GDPR.Actions.hideAll();
            if (!GDPR.Actions.isSaved()) {
                GDPR.Actions.showBar();
            }
        }
    }
</script>

<!-- GDPR TEMPLATES: Change if is necesary -->
<script id="GDPR_TEMPLATE__PRIVACY_BAR" type="text/html">
    <div id="gdpr-privacy-bar">
        <div class="gdpr-privacy-bar-title">
            #{gdpr-privacy-bar-title}
        </div>
        <ul id="gdpr-privacy-bar-list">
            #{gdpr-privacy-bar-list}
        </ul>
        <button class="gdpr-privacy-bar-more-button" onclick="GDPR.Actions.showPopup()">
            #{gdpr-privacy-bar-more-button}
        </button>
        <button class="gdpr-privacy-bar-save-button" onclick="GDPR.Actions.saveConsent()">
            #{gdpr-privacy-bar-save-button}
        </button>
        <button class="gdpr-privacy-bar-close-button" onclick="GDPR.Actions.hideBar()">
            #{gdpr-privacy-bar-close-button}
        </button>
    </div>
</script>
<script id="GDPR_TEMPLATE__PRIVACY_BAR__INPUT" type="text/html">
    <li id="gdpr-privacy-bar-input">
        <label>
            #{gdpr-privacy-bar-input-label}
            <input type="checkbox" onchange="GDPR.Actions.toogleCookiesCheck(this)"
                   name="#{gdpr-privacy-bar-input-name}"
                   #{gdpr-privacy-bar-input-checked}
                   #{gdpr-privacy-bar-input-disabled}
            />
        </label>
    </li>
</script>
<script id="GDPR_TEMPLATE__PRIVACY_BAR_SAVED" type="text/html">
    <div id="gdpr-privacy-bar-saved">
        <div class="gdpr-privacy-bar-saved-title" onclick="GDPR.Actions.showBar()">
            #{gdpr-privacy-bar-saved-title}
        </div>
        <button class="gdpr-privacy-bar-close-button" onclick="GDPR.Actions.hideBarSaved()">
            #{gdpr-privacy-bar-close-button}
        </button>
    </div>
</script>
<script id="GDPR_TEMPLATE__POPUP" type="text/html">
    <div id="gdpr-privacy-popup" onclick="GDPR.Actions.hidePopup()">
        <div id="gdpr-privacy-popup-container">

            <h1 class="gdpr-popup-title">Politica Cookie-uri</h1>
            <div class="gdpr-popup-content">
                <p>Pentru a asigura buna functionare a website-ului, uneori plasam in computerul tau mici fisiere cu
                   date - "cookie-uri". Prin informatiile ce urmeaza, oferim cateva explicatii in privinta plasarii si
                   utilizarii cookie-urilor pe acest site.</p>
                <h3>1. Ce trebuie sa stii despre cookie-uri ?</h3>
                <div>
                    <p>Un "Cookie" este un fisier de mici dimensiuni (format din litere si cifre), pe care un site il
                       salveaza in computerul, terminalul mobil sau alt echipament pe care il utilizezi pentru a accesa
                       respectivul site ("dispozitivul"). Datorita cookie-urilor, site-ul retine, pe o perioada de timp,
                       actiunile si preferintele tale (login, limba, dimensiunea caracterelor si alte preferinte de
                       afisare). Astfel nu mai trebuie sa le reintroduci ori de cate ori revii la site sau navighezi de
                       pe o pagina pe alta.</p>
                    <p>Cookie-ul este instalat prin solicitarea emisa de catre un web-server unui browser (ex: Internet
                       Explorer, Chrome) si nu contine programe software, virusi sau spyware si nu poate accesa
                       informatiile de pe hard-disk-ul dispozitivului.</p>
                    <p>Cookies sunt utilizate de aproape toate website-urile si nu iti afecteaza sistemul.</p>
                </div>
                <h3>2. De ce acest site utilizeaza cookie-uri ?</h3>
                <div>
                    <p>Cookie-urile nu sunt menite sa iti deterioreze dispozitivul de pe care accesezi <a
                                href="#{gdpr-privacy-popup-url}" target="_blank">#{gdpr-privacy-popup-name}</a>.</p>
                    <p>Daca vrei sa verifici sau sa modifici tipurile de cookie-uri pe care le accepti, poti de regula
                       sa utilizezi setarile browser-ului pe care il utilizezi pentru accesarea <a
                                href="#{gdpr-privacy-popup-url}" target="_blank">#{gdpr-privacy-popup-name}</a>. Iti
                       oferim si noi mai jos detalii despre optiunile tale.</p>
                    <p>Puteti sterge sau bloca cookie-urile noastre; insa, daca faci acest lucru, s-ar putea ca anumite
                       caracteristici ale website-ului <a href="#{gdpr-privacy-popup-url}" target="_blank">#{gdpr-privacy-popup-name}</a>
                       sa nu functioneze corespunzator.&nbsp; &nbsp;</p>
                    <p>Activarea cookie-urilor nu este strict necesara pentru functionarea site-ului
                       #{gdpr-privacy-popup-name}, dar iti poate imbunatati experienta de navigare pe <a
                                href="#{gdpr-privacy-popup-url}" target="_blank">#{gdpr-privacy-popup-name}</a>.</p>
                    <p>Stocarea de cookie-uri pe dispozitivul tau ne ofera o modalitate facila si convenabila pentru a
                       personaliza sau a imbunatati experienta ta pe #{gdpr-privacy-popup-name} si pentru a-ti face
                       urmatoarea vizita pe #{gdpr-privacy-popup-name} mai placuta.</p>
                    <p>Pe <a href="#{gdpr-privacy-popup-url}" target="_blank">#{gdpr-privacy-popup-name}</a> folosim
                       diferite tipuri de cookie-uri pentru <strong><u>diferite scopuri</u></strong>, precum:</p>
                    <div style="margin-left:18.0pt">
                        <ul>
                            <li>pentru a sti cand te autentifici pe <a href="#{gdpr-privacy-popup-url}" target="_blank">#{gdpr-privacy-popup-name}</a>;
                            </li>
                            <li>pentru a intelege mai bine modul in care utilizezi website-ul <a
                                        href="#{gdpr-privacy-popup-url}" target="_blank">#{gdpr-privacy-popup-name}</a>,
                                precum si serviciile pe care le accesezi prin intermediul acestuia;
                            </li>
                            <li>pentru asigurarea unei experiente placute la navigarea ta pe <a
                                        href="#{gdpr-privacy-popup-url}" target="_blank">#{gdpr-privacy-popup-name}</a>
                                (ex. memorarea setarilor tale personale pe paginile de web va asigura ca vei putea
                                vizita pagini web fara a trebui sa efectuezi din nou, de fiecare data, toate setarile);
                            </li>
                            <li>pentru imbunatatirea utilizarii si functionalitatii website-ului <a
                                        href="#{gdpr-privacy-popup-url}" target="_blank">#{gdpr-privacy-popup-name}</a>;
                            </li>
                            <li>pentru analiza traficului pe <a href="#{gdpr-privacy-popup-url}" target="_blank">#{gdpr-privacy-popup-name}</a>;
                            </li>
                            <li>in scopuri publicitare;</li>
                        </ul>
                    </div>
                    <p>Cookie-urile nu sunt insa utilizate pentru a executa independent programe sau de a incarca virusi
                       pe dispozitivele vizitatorilor website-ului <a href="#{gdpr-privacy-popup-url}" target="_blank">#{gdpr-privacy-popup-name}</a>.
                    </p>
                    <p>Noi NU folosim informatiile asociate cookie-urilor pentru a va identifica personal. Prin
                       cookie-uri NU urmarim sa colectam informatii personale despre vizitatorii website-ului
                       #{gdpr-privacy-popup-name} (ex. numele tau);</p>
                    <p>Cookie-urile nu sunt folosite pe website-ul #{gdpr-privacy-popup-name} in alte scopuri decat cele
                       descrise aici.&nbsp;</p>
                    <p>Prin cookie-urile de pe #{gdpr-privacy-popup-name} nu urmarim utilizatorii cand trec pe
                       website-uri ale unor terti.</p>
                    <p>In tabelul de la finalului acestei sectiuni vei gasi lista cookie-urilor pe care le utilizam pe
                       #{gdpr-privacy-popup-name}, scopurile pentru care le utilizam si din ce categorie fac parte.&nbsp;</p>
                    <p>Te rugam sa continui sa citesti pentru a afla mai multe despre cookie-urile pe care le utilizam
                       pe <a href="#{gdpr-privacy-popup-url}" target="_blank">#{gdpr-privacy-popup-name}</a>.</p>
                    <p>Te rugam sa continui sa citesti pentru a afla mai multe despre cookie-urile pe care le utilizam
                       pe <a href="#{gdpr-privacy-popup-url}" target="_blank">#{gdpr-privacy-popup-name}</a>.</p>
                </div>
                <h3>3. Ce fel de informatii colectam pe #{gdpr-privacy-popup-name} prin utilizarea cookie-urilor ?</h3>
                <div>
                    <p>Cand vizitezi website-ul <a href="#{gdpr-privacy-popup-url}" target="_blank">#{gdpr-privacy-popup-name}</a>,
                       colectam urmatoarele tipuri de informatii/date:</p>
                    <div style="margin-left:18.0pt">
                        <ul>
                            <li>adresa de protocol Internet (IP);</li>
                            <li>informatiile tale de conectare;</li>
                            <li>setarea fusului orar;</li>
                            <li>sistemul de operare;</li>
                            <li>informatii despre vizitele dvs. pe website-ul #{gdpr-privacy-popup-name};</li>
                            <li>tara de pe teritoriul careia accesezi website-ul #{gdpr-privacy-popup-name};</li>
                            <li>termenii de cautare pe care i-ai utilizat pe website-ul #{gdpr-privacy-popup-name};</li>
                            <li>produsele pe care le-ai vizualizat sau cautat pe website-ul
                                #{gdpr-privacy-popup-name};
                            </li>
                            <li>timpii de incarcare ai paginii;</li>
                            <li>erorile de descarcare;</li>
                            <li>durata vizitelor pe paginile website-ului #{gdpr-privacy-popup-name};</li>
                            <li>informatiile despre interactiunea paginii.</li>
                            <li>dispozitivul de pe care te-ai conectat</li>
                        </ul>
                    </div>
                    <h3>4. Ce categorii de cookie-uri utilizam pe #{gdpr-privacy-popup-name} ?</h3>
                    <p>Asa-numitele ½first party cookies½ sunt cookie-uri plasate de catre entitatea care opereaza
                       domeniul prin care cookie-ul este furnizat. Cookie-urile proprii #{gdpr-privacy-popup-name} sunt,
                       de aceea, de tipul ½<em><strong>first party cookies</strong>½</em>. Cookie-urile plasate de alte
                       entitati decat entitatea care opereaza website-ul <a href="#{gdpr-privacy-popup-url}"
                                                                            target="_blank">#{gdpr-privacy-popup-name}</a>
                       se numesc “<strong><em>third party cookies</em></strong>”.</p>
                    <p>Fisierele cookie pot fi sub forma de cookie-uri de sesiune sau cookie-uri fixe (numite si
                       “permanente”). Cookie-urile de sesiune vor exista numai pana inchizi browser-ul, fiind sterse
                       automat de pe dispozitiv cand inchizi browser-ul web. Cookie-urile fixe vor ramane stocate pe
                       dispozitivul utilizat pentru accesarea #{gdpr-privacy-popup-name} pana cand vor fi sterse sau
                       pana cand vor ajunge la data de expirare. Ne straduim sa plasam sau sa permitem plasarea de
                       cookie-uri fixe cu o durata de viata de maxim 2 ani. Un cookie va avea o durata de viata mai
                       lunga numai in circumstante exceptionale, cum ar fi din motive de securitate si cand este absolut
                       necesar.</p>
                    <p>Dupa scopul lor, pe <a href="#{gdpr-privacy-popup-url}" target="_blank">#{gdpr-privacy-popup-name}</a>
                       utilizam urmatoarele tipuri de cookie-uri:</p>
                    <p><strong><u>Cookie-uri tehnice (strict necesare).</u></strong> Folosim cookie-uri tehnice pentru:
                                                                                     (i) a-ti afisa website-ul nostru,
                                                                                     (ii) a crea contul tau de
                                                                                     utilizator pe
                                                                                     #{gdpr-privacy-popup-name}, (iii)
                                                                                     pentru a te autentifica atunci cand
                                                                                     intri pe contul creat pe
                                                                                     #{gdpr-privacy-popup-name} si (iv)
                                                                                     pentru a administra achizitiile
                                                                                     tale online. Aceste cookie-uri
                                                                                     tehnice sunt absolut necesare
                                                                                     pentru ca website-ul nostru sa
                                                                                     functioneze corespunzator si nu pot
                                                                                     fi dezactivate. Fara aceste
                                                                                     cookie-uri, functiile de magazin
                                                                                     online ale
                                                                                     #{gdpr-privacy-popup-name} nu pot
                                                                                     fi realizate. Aceste cookie-uri nu
                                                                                     colecteaza datele tale personale
                                                                                     sau alte date care ar putea fi
                                                                                     utilizate pentru marketing sau
                                                                                     pentru a te monitoriza cand ai
                                                                                     accesat internetul.</p>
                    <p><strong><u>Cookie-uri de functionalitate</u></strong>: Folosim aceste cookie-uri pentru a te
                                                                            ajuta sa utilizezi in mod eficient
                                                                            website-ul #{gdpr-privacy-popup-name} si
                                                                            aplicatiile noastre. De exemplu, putem
                                                                            folosi aceste cookie-uri pentru a te
                                                                            recunoaste cand revii pe website-ul nostru,
                                                                            astfel incat sa nu fie nevoie sa va
                                                                            introduci numele de utilizator de fiecare
                                                                            data cand vizitezi website-ul nostru.
                                                                            Totusi, pentru ca parola ta va ramane mereu
                                                                            criptata, din motive de securitate, va
                                                                            trebui sa introduci parola de autentificare
                                                                            de fiecare data. Aceste cookie-uri pot fi,
                                                                            de asemenea, utilizate pentru a-ti furniza
                                                                            servicii/functionalitati pe care le-ai
                                                                            solicitat, precum accesul la un anumit
                                                                            continut sau vizualizarea unui anumit
                                                                            video/unei anumite fotografii.&nbsp;
                                                                            Informatia pe care o colecteaza aceste
                                                                            cookie-uri este de regula anonimizata.
                                                                            Aceste cookie-uri functionale nu sunt
                                                                            absolut necesare pentru buna functionare a
                                                                            website-ului #{gdpr-privacy-popup-name}, dar
                                                                            ele adauga un plus de functionalitate si
                                                                            imbunatatesc experienta ta pe website-ul
                                                                            nostru.</p>
                    <p><strong><u>Cookie-uri analitice</u></strong>: Folosim aceste cookie-uri pentru a vedea cum
                                                                   utilizeaza vizitatorii #{gdpr-privacy-popup-name}, de
                                                                   exemplu care este sursa traficului pe
                                                                   #{gdpr-privacy-popup-name}, ce pagini sunt vizitate
                                                                   mai des, ce mesaje de eroare apar la pagini web, sa
                                                                   vedem care sectiuni ale website-ului sunt cele mai
                                                                   accesate; de pe ce pagini ai iesit si pe ce pagini ai
                                                                   intrat, ce tip de platforma ai folosit, numarul de
                                                                   clicuri date pe o anumita pagina, miscarile
                                                                   mouse-ului si activitatea de derulare a paginii,
                                                                   cuvintele-cheie folosite pentru cautare etc. Astfel,
                                                                   cu ajutorul acestora putem sa aflam ce functioneaza
                                                                   si ce nu, sa optimizam si sa imbunatatim website-ul
                                                                   si aplicatiile noastre, sa intelegem eficacitatea
                                                                   reclamelor si comunicarilor noastre si sa fim siguri
                                                                   ca suntem in continuare interesanti si relevanti.
                                                                   Totodata, la #{gdpr-privacy-popup-name} folosim
                                                                   cookie-uri analitice ca parte a campaniilor noastre
                                                                   de publicitate online pentru a afla cum
                                                                   interactioneaza utilizatorii cu website-ul sau
                                                                   aplicatiile noastre dupa ce au vazut o reclama
                                                                   online. Cookie-urile analitice pot include reclame pe
                                                                   website-uri ale unei terte parti. Aceste cookie-uri
                                                                   ne permit sa monitorizam si sa imbunatatim
                                                                   functionarea website-ului <a
                                href="#{gdpr-privacy-popup-url}" target="_blank">#{gdpr-privacy-popup-name}</a>.</p>
                    <p><strong><u>Cookie-uri de marketing</u></strong> Folosim cookie-urile noastre (<strong><em>first
                                                                                                                 party
                                                                                                                 cookies</em></strong>)
                                                                       si ale unor terti (<strong><em>third party
                                                                                                      cookies</em></strong>)
                                                                       pentru a va arata reclame personalizate pe <a
                                href="#{gdpr-privacy-popup-url}" target="_blank">#{gdpr-privacy-popup-name}</a> si pe
                                                                       alte website-uri. Acest proces se numeste “<em>retargeting</em>”
                                                                       si se bazeaza pe activitatile dvs. de navigare pe
                                                                       #{gdpr-privacy-popup-name}, cum ar fi:
                                                                       vizualizarea paginilor de detaliu produs,
                                                                       adaugarea in cos. Aceste cookie-uri ne ajuta sa
                                                                       iti prezentam cu prioritate oferte/informatii
                                                                       privind produse si/sau servicii de pe
                                                                       #{gdpr-privacy-popup-name} care credem ca te-ar
                                                                       putea interesa. Acestea sunt persistente (dureaza
                                                                       cat timp esti inregistrat pe
                                                                       #{gdpr-privacy-popup-name}) si presupun ca atunci
                                                                       cand te autentifici pe website sau revii pe
                                                                       website, vei vedea oferte/informatii pentru
                                                                       produse/servicii pe care le-ai
                                                                       vizualizat/achizitionat la o sesiune anterioara
                                                                       de autentificare.</p>
                    <p>Pentru a vedea un tabel cu ce cookie-uri utilizam pe <a href="#{gdpr-privacy-popup-url}"
                                                                               target="_blank">#{gdpr-privacy-popup-name}</a>
                       dati click <a href="#{gdpr-privacy-popup-cookies-policy}">aici</a></p>
                </div>
            </div>

        </div>
    </div>
</script>

<!-- GDPR CSS: Change if is necesary -->
<style>

    #GDPR_TEMPLATE__CONTENT {
        position: absolute;
        height: 0;
        width: 0;
        overflow: visible;
    }

    #gdpr-privacy-bar {
        position: fixed;
        display: none;
        bottom: 0;
        left: 0;
        right: 0;
        margin: auto;
        min-height: 60px;
        background: rgba(0, 0, 0, 0.8);
        z-index: 999998;
    }

    #gdpr-privacy-bar .gdpr-privacy-bar-title {
        padding: 5px 10px;
        color: #ffffff;
        font-size: 17px;
    }

    #gdpr-privacy-bar ul {
        display: inline-block;
        padding: 5px 10px;
        margin: 0;
        list-style-type: none;
    }

    #gdpr-privacy-bar ul li {
        display: inline-block;
        margin-right: 15px;

    }

    #gdpr-privacy-bar ul li label {
        color: #ffffff;
        font-size: 17px;
        cursor: pointer;
        line-height: 17px;
        height: 17px;
    }

    #gdpr-privacy-bar ul li label input {
        padding: 0;
        margin: 0;
    }

    #gdpr-privacy-bar button {
        display: inline-block;
    }

    #gdpr-privacy-bar-saved {
        position: fixed;
        display: none;
        bottom: 0;
        left: 0;
        right: 0;
        margin: auto;
        min-height: 60px;
        background: rgba(0, 0, 0, 0.8);
        z-index: 999998;
    }

    #gdpr-privacy-bar-saved .gdpr-privacy-bar-saved-title {
        display: inline-block;
        padding: 5px 10px;
        color: #ffffff;
        font-size: 17px;
        cursor: pointer;
    }

    #gdpr-privacy-bar-saved button {
        display: inline-block;
    }

    #gdpr-privacy-popup {
        position: fixed;
        display: none;
        bottom: 0;
        left: 0;
        right: 0;
        margin: auto;
        top: 0;
        height: 100%;
        width: 100%;
        background: rgba(0, 0, 0, 0.5);
        z-index: 999999;
    }

    #gdpr-privacy-popup #gdpr-privacy-popup-container {
        position: absolute;
        display: block;
        margin: auto;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        background: #ffffff;
        color: #000000;
        overflow-y: scroll;;
        width: 800px;
        height: 400px;
        padding: 20px 40px;
        border: 2px solid #000000;

    }
</style>

<!-- GDPR CONTENT: Don't change !!! -->
<div id="GDPR_TEMPLATE__CONTENT"></div>