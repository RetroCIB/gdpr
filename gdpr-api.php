<?php

// +-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-+

class GDPR
{
    private static $sql = null;
    private static $GDPR_CONFIG = null;

    public function __construct($GDPR_CONFIG)
    {
        self::$GDPR_CONFIG = $GDPR_CONFIG;
        //conectare la baza de date
        self::connectDatabase();
        //creeare baza de date
        self::createTable();
    }

    public function __destruct()
    {
        if (!is_null(self::$sql)) {
            mysqli_close(self::$sql);
        }
    }


    /**
     * @throws Exception
     */
    private function connectDatabase()
    {

        mysqli_report(MYSQLI_REPORT_STRICT | MYSQLI_REPORT_ALL);
        try {
            self::$sql = mysqli_connect(self::$GDPR_CONFIG['host'], self::$GDPR_CONFIG['user'], self::$GDPR_CONFIG['password']);
            mysqli_select_db(self::$sql, self::$GDPR_CONFIG['database']);
        } catch (mysqli_sql_exception  $e) {
            throw new Exception("Eroare conectare la baza de date GDPR");
        }
    }


    /**
     * @throws Exception
     */
    private function createTable()
    {

        mysqli_report(MYSQLI_REPORT_STRICT | MYSQLI_REPORT_ALL);

        try {
            $query = /** @lang:text */
                "
					CREATE TABLE IF NOT EXISTS " . self::$GDPR_CONFIG['table'] . " (
	 					id 					int(10)         UNSIGNED NOT NULL AUTO_INCREMENT,
						user_id 			int(10)         UNSIGNED NOT NULL,
						gdpr_id 			varchar(36)     NOT NULL,
						full_name 			varchar(254)    NOT NULL,
						email 				varchar(254)    NOT NULL,
						browser_agent		text            NOT NULL,
						type				varchar(254)    NOT NULL,
						consent_for 		text            NOT NULL,
						consent_html 		longtext        NOT NULL,
						accepted_at 		int(11)         NOT NULL,						
						PRIMARY KEY (id),
						KEY consents_user_id_index (user_id)
					) COLLATE " . self::$GDPR_CONFIG['charset_collate'];
            mysqli_query(self::$sql, $query);

        } catch (mysqli_sql_exception $e) {
            throw new Exception("Eroare creare tabel GDPR");
        }
    }

    /**
     * @param $user
     * @throws Exception
     */
    public function saveConsent($user)
    {

        mysqli_report(MYSQLI_REPORT_STRICT | MYSQLI_REPORT_ALL);

        foreach ($user as $key => $value) {
            $user[$key] = mysqli_real_escape_string(self::$sql, $value);
        }

        try {
            $query = /** @lang:text */
                "
                INSERT INTO " . self::$GDPR_CONFIG['table'] . "
                (user_id, gdpr_id, full_name, email, browser_agent, type, consent_for, consent_html, accepted_at) 
                VALUES 
                ( " . $user['user_id'] . ", '" . $user['gdpr_id'] . "', '" . $user['firstname'] . " " . $user['lastname'] . "', '" .
                $user['email'] . "', '" . $user['browser_agent'] . "', '" . $user['type'] . "', '" .
                $user['consent_for'] . "', '" . $user['consent_html'] . "', " . $user['accepted_at'] . ")";

            $query = mysqli_query(self::$sql, $query);

        } catch (mysqli_sql_exception $e) {
            throw new Exception("Eroare inserare in tabel GDPR");
        }
    }
}

// +-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-+

$GDPR_CONFIG = [
    'host' => 'localhost',
    'user' => 'root',
    'password' => '',
    'database' => 'gdpr_test',
    'table' => 'gdpr_consents',
    'charset_collate' => 'utf8_unicode_ci'
];

$GDPR = new GDPR($GDPR_CONFIG);

// +-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-+

$error = "gdpr_error";

if (isset($_POST['action']) && $_POST['action'] == 'register_consent') {

    // aici userul se pate transmite prin sesiune sau ceva gen
    // intrucat nu am avut nevoie de user, am lasat caamp gol
    $consent_for = json_decode(isset($_POST['gdpr_consent']) ? $_POST['gdpr_consent'] : '', true);

    $user = isset($user) ? $user : null;

    $user['user_id'] = isset($user['user_id']) ? $user['user_id'] : 0;
    $user['firstname'] = isset($user['firstname']) ? $user['firstname'] : 'Visitor';
    $user['lastname'] = isset($user['lastname']) ? $user['lastname'] : '';
    $user['email'] = isset($user['email']) ? $user['email'] : '';

    $user['gdpr_id'] = isset($consent_for['ID']) ? $consent_for['ID'] : 'Unknown';
    $user['consent_for'] = json_encode($consent_for);
    $user['consent_html'] = '';
    $user['type'] = 'cookie';
    $user['accepted_at'] = time();
    $user['browser_agent'] = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : NULL;

    try {
        $GDPR->saveConsent($user);
        $error = "gdpr_saved";
    } catch (Exception $e) {
        $error = "gdpr_error";
    }

}

echo $error;

// +-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-+