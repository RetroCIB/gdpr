<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>GDPR</title>
</head>

<body>

    <button onclick="GDPR.Actions.isSaved() ? GDPR.Actions.showBarSaved() : GDPR.Actions.showBar()">
        Afiseaza Cookie Bar
    </button>

    <div style="margin:50px">
        <?php
        for ($i = 0; $i < 2000; $i++) {
            echo substr(str_shuffle("abcdefghijklmnopqrstuvwxyz"), 0, rand(2, 12)) . " ";
        }
        ?>
    </div>

    <?php require_once "gdpr.class.php"; ?>

</body>
</html>
