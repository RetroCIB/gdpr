**index.php**  
--

-- se adauga butonul in pagina pentru cookie-policy pentru afisare bara  

    <button onclick="GDPR.Actions.isSaved() ? GDPR.Actions.showBarSaved() : GDPR.Actions.showBar()">Afiseaza Cookie Bar</button>
    
-- se adauga injectare script in subsolul paginii

    <?php require_once "gdpr.class.php"; ?>
    
    
**gdpr.class.php**
--      

_se pot modifica urmatoarele campuri $GDPR_OPTIONS:_

-- calea catre api  

    'API' => 'gdpr-api.php',

-- UA pentru google analitics daca este folosit in cookies
    
    'UA' => 'UA-120261035-1',
  
-- tipurile de cookies pentru care se doreste consimtamantul 
    
    'cookies' => [
            'technical' => [
                'label' => 'Cookie-uri tehnice (strict necesare)',
                'name' => 'technical',
                'checked' => true,
                'disabled' => true,
                'cookies_names' => ['_ga', '_gid']
            ],
            'functional' => [
                'label' => 'Cookie-uri de functionalitate',
                'name' => 'functional',
                'checked' => true,
                'disabled' => false,
                'cookies_names' => ['_ga', '_gid']
            ],
            'analytical' => [
                'label' => 'Cookie-uri analitice',
                'name' => 'analytical',
                'checked' => true,
                'disabled' => false,
                'cookies_names' => ['_ga', '_gid']
            ],
            'marketing' => [
                'label' => 'Cookie-uri de marketing',
                'name' => 'marketing',
                'checked' => false,
                'disabled' => false,
                'cookies_names' => ['_ga', '_gid']
            ]
        ],
        
-- link-urile catre site-ul la care se face referinta in popup, precum si catre pagina de cookie-policy           
        
    'popup' => [
        'url' => 'http://retrocib.com',
        'name' => 'RetroCIB',
        'cookies_policy' => "http://retrocib.com/cookies-policy"
    ],

-- titlul pentru bara si mesajele pentru butoanele: more, save, close
    
    'bar' => [
        'title' => 'Acest website foloseste cookies pentru a va oferi cea mai buna experienta de utilizare. Va rugam sa va exprimati preferintele in ceea ce priveste utilizarea cookie-urilor.',
        'more_button' => 'More',
        'save_button' => 'Save',
        'close_button' => 'Close'
    ],
    
-- titlul pentru bara in cazul in care deja s-a acordat consimtamantul anterior si mesajul pentru butonul close
    
    'bar_saved' => [
        'title' => 'Acest site foloseste cookie',
        'close_button' => 'Close'
    ]
    
_se pot modifica scripturile template dar se va tine cont de identificatori:_    
    
-- GDPR_TEMPLATE__PRIVACY_BAR

    #{gdpr-privacy-bar-title}
    #{gdpr-privacy-bar-list}
    #{gdpr-privacy-bar-more-button}
    #{gdpr-privacy-bar-save-button}
    #{gdpr-privacy-bar-close-button}

-- GDPR_TEMPLATE__PRIVACY_BAR__INPUT

    onchange="GDPR.Actions.toogleCookiesCheck(this)"

    #{gdpr-privacy-bar-input-label}
    #{gdpr-privacy-bar-input-name}
    #{gdpr-privacy-bar-input-checked}
    #{gdpr-privacy-bar-input-disabled}
    
-- GDPR_TEMPLATE__PRIVACY_BAR_SAVED

    onclick="GDPR.Actions.showBar()"
    
    #{gdpr-privacy-bar-saved-title}
    
    onclick="GDPR.Actions.hideBarSaved()"
    
    #{gdpr-privacy-bar-close-button}
    
-- GDPR_TEMPLATE__POPUP (se poate modifica textul)

    onclick="GDPR.Actions.hidePopup()"

    #{gdpr-privacy-popup-url}
    #{gdpr-privacy-popup-name}
    #{gdpr-privacy-popup-cookies-policy}"

    
**gdpr-api.php**
--

-- se modifica $GDPR_CONFIG pentru accesare baza de date

    $GDPR_CONFIG =[
        'host' => 'localhost',
        'user' => 'root',
        'password' => '',
        'database' => 'gdpr_test',
        'table' => 'gdpr_consents',
        'charset_collate' => 'utf8_unicode_ci'
    ];    
    
-- se modifica userul in functie de login daca e nevoie ( eu as injecta scriptul in controller sau as prelua userul din sesiune)

    $user = isset($user) ? $user : null;
    
        